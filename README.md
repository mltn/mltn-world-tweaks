## MltnWorldTweaks
### Usage
Download a release from [releases](https://gitlab.com/mltn/mltn-world-tweaks/-/releases), and put it in your VRChat/Mods.

[MelonLoader](https://melonwiki.xyz) is required.

### Supported Worlds
| World Name     | World ID                                  | Tweaks                                                                                                                               | Source                                                      |
|----------------|-------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------|
| MOVIE ＆ CHILL  | wrld_791ebf58-54ce-4d3a-a0a0-39f10e1b20b2 | Remove all posters except for the entrance one, remove the photo viewer, remove pillow textures except for p6, remove supporter hats | [MovieAndChill.cs](MltnWorldTweaks/Worlds/MovieAndChill.cs) |


## Examples
### MovieAndChill
| Stock                                              | Patched                                              |
|----------------------------------------------------|------------------------------------------------------|
| ![MOVIE & CHILL Example 1](images/mac_1_stock.jpg) | ![MOVIE & CHILL Example 1](images/mac_1_patched.jpg) |
| ![MOVIE & CHILL Example 2](images/mac_2_stock.jpg) | ![MOVIE & CHILL Example 2](images/mac_2_patched.jpg) |
| ![MOVIE & CHILL Example 3](images/mac_3_stock.jpg) | ![MOVIE & CHILL Example 3](images/mac_3_patched.jpg) |