﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using HarmonyLib;
using MelonLoader;
using MltnTweakCore;
using Newtonsoft.Json;
using UnityEngine.Networking;
using VRC.Core;

namespace MltnWorldTweaks
{
    public class WorldTweaks : MelonMod
    {
        private static WorldTweaks Instance;
        private static string baseUrl = "https://gitlab.com/mltn/mltn-world-tweaks-data/-/raw/main/";

        public override void OnApplicationStart()
        {
            Instance = this;
            HarmonyInstance.Patch(
                typeof(RoomManager).GetMethod("Method_Public_Static_Boolean_ApiWorld_ApiWorldInstance_String_Int32_0"),
                null,
                new HarmonyMethod(typeof(WorldTweaks).GetMethod(nameof(OnInstanceChange),
                    BindingFlags.NonPublic | BindingFlags.Static)));
        }

        private static void OnInstanceChange(ApiWorld __0, ApiWorldInstance __1)
        {
            if (__0 == null) return;
            Instance.ApplyWorldPatch(__0.id);
        }
        
        public void ApplyWorldPatch(string worldId)
        {
            MelonCoroutines.Start(FetchPatches(worldId));
        }

        private static IEnumerator FetchPatches(string worldId)
        {
            var patchesUrl = baseUrl + "Patches.json";
            MelonLogger.Log($"Fetching patches from {patchesUrl}");
            var request = UnityWebRequest.Get(patchesUrl);
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                MelonLogger.LogError($"Failed to fetch patches from {patchesUrl}");
                yield break;
            }

            var patches = JsonConvert.DeserializeObject<Dictionary<string, PatchRoute>>(request.downloadHandler.text);
            MelonLogger.Log($"Fetched {patches.Count} patches");
            if (!patches.ContainsKey(worldId))
            {
                MelonLogger.Log($"No patches found for world {worldId}");
                yield break;
            }
            
            var patchRoute = patches[worldId];
            MelonLogger.Log($"Fetching patch for {worldId}");
            
            var patchUrl = baseUrl + "Patches"+ patchRoute.patch;
            
            var patchRequest = UnityWebRequest.Get(patchUrl);
            yield return patchRequest.SendWebRequest();
            if (patchRequest.isNetworkError || patchRequest.isHttpError)
            {
                MelonLogger.LogError($"Failed to fetch patch from {patchUrl}");
                yield break;
            }
            
            var patch = patchRequest.downloadHandler.text;
            MelonLogger.Log($"Fetched patch for {worldId}");
            MelonLogger.Log($"Applying patch for {worldId}");
            Patcher.ApplyWorldPatch(patch);
        }
    }
}